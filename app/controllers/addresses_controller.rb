class AddressesController < ActionController::API
    
    def show
        address = Address.find(params[:id])
    end 

    def create
        success = false
        address = Address.new(address_params)
        if address.save
            sender = Sender.new(address_id: address.id)
            success = true if sender.save
        else
          render json: { errors: address.errors.full_messages }, status: :bad_request
        end
        if success 
            render json: {status: 'address created successfully'}, status: :created
        else 
            render('Request Cancelled')
        end 
    end 

    def address_params
        params.require(:address).permit(:streetname, :zipcode, :city, :country)
    end
end